from django.apps import AppConfig


class ExproutingConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'exprouting'
